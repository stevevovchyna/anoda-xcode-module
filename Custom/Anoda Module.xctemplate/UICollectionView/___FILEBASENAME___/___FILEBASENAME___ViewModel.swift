//___FILEHEADER___

import Foundation
import Combine

protocol ___FILEBASENAMEASIDENTIFIER___Delegate: class {
    func didUpdate(props: ___VARIABLE_productName:identifier___Props)
}

class ___FILEBASENAMEASIDENTIFIER___ {
    
    weak var delegate: ___FILEBASENAMEASIDENTIFIER___Delegate?
    
    private var cancellableBag: Set<AnyCancellable> = []
    
    var props = ___VARIABLE_productName:identifier___Props()
    
    func viewDidLoad() {
        self.delegate?.didUpdate(props: props)
    }
}
