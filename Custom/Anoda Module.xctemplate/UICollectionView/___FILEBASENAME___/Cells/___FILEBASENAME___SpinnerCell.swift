//___FILEHEADER___

import UIKit

class ___FILEBASENAMEASIDENTIFIER___: UICollectionViewCell {
    
    let spinner = UIActivityIndicatorView(style: .large)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(active: Bool) {
        if active {
            spinner.startAnimating()
        } else {
            spinner.stopAnimating()
        }
    }
}

// MARK: - Configure
private extension ___FILEBASENAMEASIDENTIFIER___ {
    func configureView() {
        contentView.backgroundColor = RColor.white()
        
        contentView.addSubview(spinner)
        spinner.snp.makeConstraints {
            $0.top.bottom.equalToSuperview().inset(20)
            $0.center.equalToSuperview()
        }
    }
}
