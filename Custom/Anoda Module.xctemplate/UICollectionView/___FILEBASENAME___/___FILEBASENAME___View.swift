//___FILEHEADER___

import UIKit

protocol ___FILEBASENAMEASIDENTIFIER___Delegate: class {
    
}

class ___FILEBASENAMEASIDENTIFIER___: UIView {
    
    weak var delegate: ___FILEBASENAMEASIDENTIFIER___Delegate?
    
    private lazy var collectionView: UICollectionView = {
        return UICollectionView(frame: .zero, collectionViewLayout: createLayout())
    }()
    private var dataSource: ___VARIABLE_productName:identifier___DataSource?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
        configureСollectionView()
        configureDataSource()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func show(props: ___VARIABLE_productName:identifier___Props) {
        dataSource?.apply(props: props)
    }
}

// MARK: - Configure
private extension ___FILEBASENAMEASIDENTIFIER___ {
    func configureView() {
        backgroundColor = RColor.white()
    }
    
    func configureСollectionView() {
        backgroundColor = RColor.white()
        
        addSubview(collectionView)
        collectionView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
        collectionView.backgroundColor = RColor.white()
        collectionView.alwaysBounceVertical = true
        collectionView.register(cell: ___VARIABLE_productName:identifier___SpinnerCell.self)
    }
    
    func configureDataSource() {
        dataSource = ___VARIABLE_productName:identifier___DataSource(collectionView: collectionView) { (collectionView, indexPath, item) in
            switch item {
            case .spinner:
                guard let cell: ___VARIABLE_productName:identifier___SpinnerCell = collectionView.dequeueReusableCell(for: indexPath) else { break }
                return cell
            }
            return UICollectionViewCell()
        }
    }
    
    func createLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { [weak self] sectionIndex, layoutEnvironment -> NSCollectionLayoutSection? in
            guard let sectionIdentifiers = self?.dataSource?.snapshot().sectionIdentifiers else {
                return nil
            }
            return sectionIdentifiers[safe: sectionIndex]?.layoutKind.section(layoutEnvironment: layoutEnvironment)
        }
        return layout
    }
}

//MARK: - SectionIdentifierType, ItemIdentifierType
extension ___FILEBASENAMEASIDENTIFIER___ {
    enum Section {
        case main
        
        fileprivate var layoutKind: SectionLayoutKind {
            switch self {
            case .main:
                return .list(heightDimension: .estimated(44))
            }
        }
    }
    
    enum Item: Hashable {
        case spinner
        
        func hash(into hasher: inout Hasher) {
            switch self {
            case .spinner:
                hasher.combine("spinner")
            }
        }
    }
}

// MARK: - SectionLayoutKind
private extension ___FILEBASENAMEASIDENTIFIER___ {
    enum SectionLayoutKind {
        case list(heightDimension: NSCollectionLayoutDimension?)
        
        func section(layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection {
            switch self {
            case .list(let heightDimension):
                return createListSection(heightDimension: heightDimension, layoutEnvironment: layoutEnvironment)
            }
        }
        
        private func createListSection(heightDimension: NSCollectionLayoutDimension?,
                                       layoutEnvironment: NSCollectionLayoutEnvironment) -> NSCollectionLayoutSection {
            let heightDimension = heightDimension ?? .estimated(44)
            let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                  heightDimension: heightDimension)
            let item = NSCollectionLayoutItem(layoutSize: itemSize)
            
            let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1),
                                                   heightDimension: heightDimension)
            let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize,
                                                           subitem: item,
                                                           count: 1)
            let section = NSCollectionLayoutSection(group: group)
            return section
        }
    }
}
