//___FILEHEADER___

import UIKit

class ___FILEBASENAMEASIDENTIFIER___: UICollectionViewDiffableDataSource<___VARIABLE_productName:identifier___View.Section, ___VARIABLE_productName:identifier___View.Item> {
    
    typealias Section = ___VARIABLE_productName:identifier___View.Section
    typealias Item = ___VARIABLE_productName:identifier___View.Item
    typealias Props = ___VARIABLE_productName:identifier___Props
    
    private var props = Props()
    
    func apply(props: Props, animated: Bool = true) {
        self.props = props
        reloadData(animatingDifferences: animated)
    }
    
    func reload(animated: Bool = true) {
        reloadData(animatingDifferences: animated)
    }
    
    // MARK: - Data Reload
    private func reloadData(animatingDifferences: Bool = true) {
        var snapshot = NSDiffableDataSourceSnapshot<Section, Item>()
        
        snapshot.appendSection(.main)
        snapshot.appendItem(.spinner,
                            toSection: .main)
        
        DispatchQueue.main.async {
            self.apply(snapshot, animatingDifferences: animatingDifferences)
        }
    }
}
