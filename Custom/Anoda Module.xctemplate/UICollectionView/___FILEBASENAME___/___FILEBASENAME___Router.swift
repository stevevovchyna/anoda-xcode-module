//___FILEHEADER___

import UIKit
import Swinject

protocol ___VARIABLE_productName:identifier___VCRoutingProtocol {
    
}

protocol ___VARIABLE_productName:identifier___VCPresentingProtocol: class {
    func show___VARIABLE_productName:identifier___ViewController(animated: Bool)
}

class ___FILEBASENAMEASIDENTIFIER___: ModuleRouterProtocol {
    
    weak var viewController: UIViewController?
    
    var navigationController: LocaliseNavigationController? {
        get {
            return viewController?.navigationController as? LocaliseNavigationController
        }
    }
    
    private let resolver: Swinject.Resolver
    
    init(resolver: Swinject.Resolver) {
        self.resolver = resolver
    }
}

extension ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_productName:identifier___VCRoutingProtocol {
    
}
