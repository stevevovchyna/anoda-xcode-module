//___FILEHEADER___

import Foundation
import Swinject
import SwinjectAutoregistration

class ___FILEBASENAMEASIDENTIFIER___: ModuleBuilder {
    typealias Controller = ___VARIABLE_productName:identifier___ViewController
    typealias ViewModel = ___VARIABLE_productName:identifier___ViewModel
    typealias Router = ___VARIABLE_productName:identifier___Router
    
    private let resolver: Swinject.Resolver
        
    init(resolver: Swinject.Resolver) {
        self.resolver = resolver
    }

    func build() -> ___VARIABLE_productName:identifier___ViewController {
        let router = Router(resolver: resolver)
        let viewModel = ViewModel()
        let viewController = Controller(viewModel: viewModel,
                                        router: router)
        router.viewController = viewController
        return viewController
    }
}
