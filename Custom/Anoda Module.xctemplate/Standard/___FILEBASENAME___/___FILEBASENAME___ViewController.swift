//___FILEHEADER___

import UIKit

class ___FILEBASENAMEASIDENTIFIER___: LocaliseViewController {
    
    private let viewModel: ___VARIABLE_productName:identifier___ViewModel
    private let router: ___VARIABLE_productName:identifier___Router
    private let contentView: ___VARIABLE_productName:identifier___View
    
    init(viewModel: ___VARIABLE_productName:identifier___ViewModel,
        router: ___VARIABLE_productName:identifier___Router) {
        self.viewModel = viewModel
        self.router = router
        self.contentView = ___VARIABLE_productName:identifier___View()
        super.init()
        self.contentView.delegate = self
        self.viewModel.delegate = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.viewDidLoad()
    }
}

// MARK: - ViewModel Delegate
extension ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_productName:identifier___ViewModelDelegate {
    
}

// MARK: - View Delegate
extension ___FILEBASENAMEASIDENTIFIER___: ___VARIABLE_productName:identifier___ViewDelegate {
    
}
