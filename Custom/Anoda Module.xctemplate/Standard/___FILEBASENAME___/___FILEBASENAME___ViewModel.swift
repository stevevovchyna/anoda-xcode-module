//___FILEHEADER___

import Foundation
import Combine

protocol ___FILEBASENAMEASIDENTIFIER___Delegate: class {

}

class ___FILEBASENAMEASIDENTIFIER___ {
    
    weak var delegate: ___FILEBASENAMEASIDENTIFIER___Delegate?
    
    private var cancellableBag: Set<AnyCancellable> = []
    
    func viewDidLoad() {
        
    }
}
