//___FILEHEADER___

import UIKit

protocol ___FILEBASENAMEASIDENTIFIER___Delegate: class {

}

class ___FILEBASENAMEASIDENTIFIER___: UIView {
    
    weak var delegate: ___FILEBASENAMEASIDENTIFIER___Delegate?
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        configureView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Configure
private extension ___FILEBASENAMEASIDENTIFIER___ {
    func configureView() {
        backgroundColor = RColor.white()
    }
}
